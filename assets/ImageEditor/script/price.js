$(document).on("click", "#buttonPrint", function(){
	var materialIndex = document.getElementById("material");
	var sizeIndex = document.getElementById("size");
	var materialPrice;
	var sizePrice;
	var totalPrice;

	var dataUrl = app.stage.toDataURL();

	var material;
	var size;
	var status;

	if(materialIndex.selectedIndex == 0){
		material = "Paper";
		materialPrice = 1;
	}else if(materialIndex.selectedIndex == 1){
		material = "Cloth";
		materialPrice = 2;
	}else if(materialIndex.selectedIndex == 2){
		material = "Skin";
		materialPrice = 3;
	}else if(materialIndex.selectedIndex == 3){
		material = "Mica";
		materialPrice = 4;
	}

	if(sizeIndex.selectedIndex == 0){
		size = "Large";
		sizePrice = 5;
	}else if(sizeIndex.selectedIndex == 1){
		size = "Medium";
		sizePrice = 4;
	}else if(sizeIndex.selectedIndex == 2){
		size = "Small";
		sizePrice = 3;
	}

	totalPrice = materialPrice + sizePrice;

	var status;
	
	var today = new Date();

	var enterDate = new Date(document.getElementById('orderDate').value);

	if (enterDate < today){
	    status = "Done";
	}
	else{
		status = "Pending";
	}

	alert(status);

	jQuery.ajax({
        type: "POST",
        url: "print.php",
        data: { material: material, size: size, price: totalPrice, status: status, image: dataUrl},
       
    });
});

